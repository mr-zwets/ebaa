awk -F ',' '{OFS=","; print $1,$5,$5}' $1 \
| ../../implementation-c/bin/aggregate -windowlength 50 \
| awk -F ',' 'NR>1 {OFS=","; print NR-2,0,$15}' \
> chunksizes-eth.csv
